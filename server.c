#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), sendto(), and recvfrom() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <pthread.h>

#include <semaphore.h>
#include <sys/sem.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define MAXPENDING 5
#define ECHOMAX 255     /* Longest string to echo */
#define Maxlen 25
#define RCVBUFSIZE 32   /* Size of receive buffer */
#define SERVERPORTTCP 6000
#define SERVERPORT 5000
#define SERVERPORTUDP 3000
#define MSGSZ 128

int globalQe = 0;

typedef struct msgbuf {
    long mtype;
    char mtext[MSGSZ];
} message_buf;

message_buf sbuf;
message_buf rbuf;
size_t buf_length;

int msqid, flag = 0, servSocktcp;
key_t semkey, key = 10;
sem_t mutex;

void DieWithError(char *errorMessage); /* External error handling function */
void *ThreadMain(void *arg);
void *udptreadwait(void *arg);
void *udptreadsend(void *arg);
void *tcptreadrecv(void *arg);
int CreateTCPServerSocket(unsigned short port);
int AcceptTCPConnection(int servSock);
void HandleTCPClient(int clntSocket);

struct ThreadArgs {
    int clntSock; /* Socket descriptor for client */
};

int main(int argc, char *argv[]) {

    int res;
    pthread_t thread1;
    pthread_t thread2;
    pthread_t thread3;
    void *thread_result;
    char str[Maxlen];
    struct sockaddr_in echoServAddr; /* Local address */
    struct sockaddr_in echoClntAddr; /* Client address */

    int msgflg = IPC_CREAT | 0666;

    if ((msqid = msgget(key, msgflg)) < 0) {
        perror("msgget");
        exit(1);
    }

    if (sem_init(&mutex, 1, 1) < 0) {
        perror("semaphore initilization");
        exit(0);
    }

    res = pthread_create(&thread1, NULL, udptreadwait, (void *) str);
    if (res != 0) {
        perror("Thread creation failed");
        exit(EXIT_FAILURE);
    }
    //---------------
    res = pthread_create(&thread2, NULL, udptreadsend, (void *) str);
    if (res != 0) {
        perror("Thread creation failed");
        exit(EXIT_FAILURE);
    }
    servSocktcp = CreateTCPServerSocket(SERVERPORTTCP);
    res = pthread_create(&thread3, NULL, tcptreadrecv, (void *) str);
    if (res != 0) {
        perror("Thread creation failed");
        exit(EXIT_FAILURE);
    }
    int servSock; /* Socket descriptor for server */
    int clntSock; /* Socket descriptor for client */
    unsigned short echoServPort; /* Server port */
    pthread_t threadID; /* Thread ID from pthread_create() */
    struct ThreadArgs *threadArgs; /* Pointer to argument structure for thread */

    echoServPort = SERVERPORT; /* First arg:  local port */

    /* Create socket for incoming connections */
    if ((servSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        DieWithError("socket() failed");
    servSock = CreateTCPServerSocket(echoServPort);
    for (;;) /* run forever */ {
        if (globalQe < 10) {

            clntSock = AcceptTCPConnection(servSock);

            /* Create separate memory for client argument */
            if ((threadArgs = (struct ThreadArgs *) malloc(sizeof (struct ThreadArgs))) == NULL)
                DieWithError("malloc() failed");
            threadArgs -> clntSock = clntSock;

            /* Create client thread */
            if (pthread_create(&threadID, NULL, ThreadMain, (void *) threadArgs) != 0)
                DieWithError("pthread_create() failed");
            printf("with thread %ld\n", (long int) threadID);
        }

    }

    res = pthread_join(thread1, &thread_result);
    if (res != 0) {
        perror("Thread join-failed");
        exit(EXIT_FAILURE);
    }

    res = pthread_join(thread2, &thread_result);
    if (res != 0) {
        perror("Thread join-failed");
        exit(EXIT_FAILURE);
    }

    res = pthread_join(threadID, &thread_result);
    if (res != 0) {
        perror("Thread join-failed");
        exit(EXIT_FAILURE);
    }
}

void *udptreadwait(void *arg) {//udp жду сообщений для клиента первого типа

    int sock; /* Socket descriptor */
    struct sockaddr_in echoServAddr; /* Echo server address */
    struct sockaddr_in fromAddr; /* Source address of echo */
    unsigned short echoServPort; /* Echo server port */
    unsigned int fromSize; /* In-out of address size for recvfrom() */
    char *servIP; /* IP address of server */
    char *echoString; /* String to send to echo server */
    char echoBuffer[ECHOMAX + 1]; /* Buffer for receiving echoed string */
    int echoStringLen; /* Length of string to echo */
    int respStringLen; /* Length of received response */

    servIP = "127.0.0.1"; /* First arg: server IP address (dotted quad) */
    echoString = "str"; /* Second arg: string to echo */

    if ((echoStringLen = strlen(echoString)) > ECHOMAX) /* Check input length */
        DieWithError("Echo word too long");

    echoServPort = 4000; /* 7 is the well-known port for the echo service */

    /* Create a datagram/UDP socket */
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        DieWithError("socket() failed");

    /* Construct the server address structure */
    memset(&echoServAddr, 0, sizeof (echoServAddr)); /* Zero out structure */
    echoServAddr.sin_family = AF_INET; /* Internet addr family */
    echoServAddr.sin_addr.s_addr = inet_addr(servIP); /* Server IP address */
    echoServAddr.sin_port = htons(echoServPort); /* Server port */
    for (;;) {
        if (globalQe < 10) {
            /* Send the string to the server */
            if (sendto(sock, echoString, echoStringLen, 0, (struct sockaddr *) &echoServAddr, sizeof (echoServAddr)) != echoStringLen) {
                DieWithError("sendto() sent a different number of bytes than expected");
            }
        }
    }
    close(sock);

    exit(0);
}

void *ThreadMain(void *threadArgs) {
    int clntSock; /* Socket descriptor for client connection */

    /* Guarantees that thread resources are deallocated upon return */
    pthread_detach(pthread_self());

    /* Extract socket file descriptor from argument */
    clntSock = ((struct ThreadArgs *) threadArgs) -> clntSock;
    free(threadArgs); /* Deallocate memory for argument */

    HandleTCPClient(clntSock);

    return (NULL);
}

void DieWithError(char *errorMessage) {
    perror(errorMessage);
    exit(1);
}

int CreateTCPServerSocket(unsigned short port) {
    int sock; /* socket to create */
    struct sockaddr_in echoServAddr; /* Local address */

    /* Create socket for incoming connections */
    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        DieWithError("socket() failed");

    /* Construct local address structure */
    memset(&echoServAddr, 0, sizeof (echoServAddr)); /* Zero out structure */
    echoServAddr.sin_family = AF_INET; /* Internet address family */
    echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
    echoServAddr.sin_port = htons(port); /* Local port */

    /* Bind to the local address */
    if (bind(sock, (struct sockaddr *) &echoServAddr, sizeof (echoServAddr)) < 0)
        DieWithError("bind() failed");

    /* Mark the socket so it will listen for incoming connections */
    if (listen(sock, MAXPENDING) < 0)
        DieWithError("listen() failed");

    return sock;
}

int AcceptTCPConnection(int servSock) {
    int clntSock; /* Socket descriptor for client */
    struct sockaddr_in echoClntAddr; /* Client address */
    unsigned int clntLen; /* Length of client address data structure */
    /* Set the size of the in-out parameter */
    clntLen = sizeof (echoClntAddr);

    /* Wait for a client to connect */
    if ((clntSock = accept(servSock, (struct sockaddr *) &echoClntAddr, &clntLen)) < 0)
        DieWithError("accept() failed");

    /* clntSock is connected to a client! */

    printf("Handling client %s\n", inet_ntoa(echoClntAddr.sin_addr));

    return clntSock;
}

void HandleTCPClient(int clntSocket) {
    char echoBuffer[RCVBUFSIZE]; /* Buffer for echo string */
    int recvMsgSize; /* Size of received message */

    sbuf.mtype = 1;
    /* Receive message from client */
    if ((recvMsgSize = recv(clntSocket, echoBuffer, RCVBUFSIZE, 0)) < 0)
        DieWithError("recv() failed");
    printf("%s\n", echoBuffer);

    strcpy(sbuf.mtext, echoBuffer);
    buf_length = strlen(sbuf.mtext) + 1;
    sem_wait(&mutex);
    if (msgsnd(msqid, &sbuf, buf_length, IPC_NOWAIT) < 0) {
        printf("%d, %ld, %s, %zd\n", msqid, sbuf.mtype, sbuf.mtext, buf_length);
        perror("msgsnd");
        exit(1);
    } else {
        printf("Message: \"%s\" Sent\n", sbuf.mtext);
        globalQe++;
    }
    sem_post(&mutex);
    close(clntSocket); /* Close client socket */
}

void *udptreadsend(void *arg) {//udp есть сообщения для клиента второго типа

    message_buf rbuf;
    int sock, i = 0;
    struct sockaddr_in ad;
    char *sendString;
    int bcastPerm = 1; /* Socket opt to set permission to broadcast */
    unsigned int sendStrlen;
    char *IP = "127.0.0.1"; /* IP broadcast address */
    //int massPort[] = {1210, 1211, 1212, 1213, 1214, 1215, 1216, 1217, 1218, 1219, 1220};
    char buf[1024];
    char str[Maxlen];
    char *echoString; /* String to send to echo server */
    int echoStringLen; /* Length of string to echo */
    sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);

    setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void *) &bcastPerm, sizeof (bcastPerm));
    memset(&ad, 0, sizeof (ad)); /* Zero out structure */
    ad.sin_family = AF_INET; /* Internet address family */
    ad.sin_addr.s_addr = htonl(INADDR_ANY); /* Broadcast IP address */
    ad.sin_port = htons(SERVERPORTUDP);

    echoString = "str"; /* Second arg: string to echo */

    if ((echoStringLen = strlen(echoString)) > ECHOMAX) /* Check input length */
        DieWithError("Echo word too long");

    for (;;) {/* Run forever */
        if (globalQe > 0) {
            sendto(sock, echoString, echoStringLen, 0, (struct sockaddr *) &ad, sizeof (ad));
        }
    }
    pthread_exit("done udp");
}

void *tcptreadrecv(void *arg) {//отправка клиенту второго типа
    int servSock, clntSock;
    int sock; /* socket to create */
    struct sockaddr_in echoServAddr; /* Local address */
    struct sockaddr_in echoClntAddr; /* Client address */
    unsigned int clntLen; /* Length of client address data structure */

    char echoBuffer[RCVBUFSIZE]; /* Buffer for echo string */
    int recvMsgSize; /* Size of received message */
    char buf[1024];
    unsigned int sendStrlen;

    /* Set the size of the in-out parameter */
    clntLen = sizeof (echoClntAddr);
    for (;;) {
        if (globalQe > 0) {
            /* Wait for a client to connect */
            if ((clntSock = accept(servSocktcp, (struct sockaddr *) &echoClntAddr, &clntLen)) < 0)
                DieWithError("accepttcp() failed");

            /* clntSock is connected to a client! */

            printf("Handling client %s\n", inet_ntoa(echoClntAddr.sin_addr));

            sem_wait(&mutex);
            if (msgrcv(msqid, &rbuf, MSGSZ, 1, 0) < 0) {
                perror("msgrcv");
                exit(1);
            } else {
                globalQe--;
            }
            sem_post(&mutex);

            strcpy(buf, rbuf.mtext);
            sendStrlen = strlen(buf) + 1;

            send(clntSock, buf, sendStrlen, 0);
            close(clntSock); /* Close client socket */
        }
    }
}