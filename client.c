#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket() and bind() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_ntoa() */
#include <netinet/in.h>
#include <sys/types.h>
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */

#define ECHOMAX 255     /* Longest string to echo */

void DieWithError(char *errorMessage); /* External error handling function */

int main(int argc, char *argv[]) {
    int sock; /* Socket */
    struct sockaddr_in echoServAddr; /* Local address */
    struct sockaddr_in echoClntAddr; /* Client address */
    unsigned int cliAddrLen; /* Length of incoming message */
    char echoBuffer[ECHOMAX]; /* Buffer for echo string */
    unsigned short echoServPort; /* Server port */
    int recvMsgSize; /* Size of received message */
    int echoStringLen;
    char* serverIP;
    char echoString[ECHOMAX];
    char* tmpstr;
    char *p;
    srand(time(NULL));
    char str[10]; // инициализация функции rand значением функции time

    int timeSleep;
    echoServPort = 4000; /* First arg:  local port */

    /* Create socket for sending/receiving datagrams */
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        DieWithError("socket() failed");

    /* Construct local address structure */
    memset(&echoServAddr, 0, sizeof (echoServAddr)); /* Zero out structure */
    echoServAddr.sin_family = AF_INET; /* Internet address family */
    echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
    echoServAddr.sin_port = htons(echoServPort); /* Local port */

    /* Bind to the local address */
    if (bind(sock, (struct sockaddr *) &echoServAddr, sizeof (echoServAddr)) < 0)
        DieWithError("bind() failed");
    cliAddrLen = sizeof (echoClntAddr);
    if ((recvMsgSize = recvfrom(sock, echoBuffer, ECHOMAX, 0, (struct sockaddr *) &echoClntAddr, &cliAddrLen)) < 0)
        DieWithError("recvfrom() failed");

    for (;;) /* Run forever */ {
        
        serverIP = inet_ntoa(echoClntAddr.sin_addr);
        printf("Handling client %s\n", serverIP);

        if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
            DieWithError("socket() failed");

        /* Construct the server address structure */
        memset(&echoServAddr, 0, sizeof (echoServAddr)); /* Zero out structure */
        echoServAddr.sin_family = AF_INET; /* Internet address family */
        echoServAddr.sin_addr.s_addr = inet_addr(serverIP); /* Server IP address */
        echoServAddr.sin_port = htons(5000); /* Server port */
        puts("preconnect");
        /* Establish the connection to the echo server */
        if (connect(sock, (struct sockaddr *) &echoServAddr, sizeof (echoServAddr)) < 0)
            DieWithError("connect() failed");
        puts("Connect success");
        memset(str, ' ', 20);
        //sprintf(tmpstr, "%d", timeSleep);
        
        //strcat(str, tmpstr);
        randStr(str);

        echoStringLen = strlen(str) + 1; /* Determine input length */

        /* Send the string to the server */
        if (send(sock, str, echoStringLen, 0) != echoStringLen)
            DieWithError("send() sent a different number of bytes than expected");
        printf("%s\n", str);
        close(sock);
        timeSleep = rand() % 4 + 1;
        sleep(timeSleep);
    }
    /* NOT REACHED */
}

void DieWithError(char *errorMessage) {
    perror(errorMessage);
    exit(1);
}

void randStr(char str[]) {
    char massStr[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'y', 's', 'm', 'k', 'z', 'l', 'o'}; //15
    int r, r1, i;
    char tmpstr[1];
    int timeSleep = rand() % 4 + 1;
    r = rand() % 8 + 4;
    sprintf(tmpstr, "%d", timeSleep);
    str[0] = tmpstr[0];
    for (i = 1; i < r; i++) {
        r1 = 0;

        r1 = rand() % 14;

        str[i] = massStr[r1];
    }
    str[r] = '\0';
}