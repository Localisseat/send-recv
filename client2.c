#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket() and bind() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_ntoa() */
#include <netinet/in.h>
#include <sys/types.h>
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */

#define ECHOMAX 255     /* Longest string to echo */
#define RCVBUFSIZE 32
void DieWithError(char *errorMessage); /* External error handling function */

int main(int argc, char *argv[]) {
    int sock; /* Socket */
    struct sockaddr_in echoServAddr; /* Local address */
    struct sockaddr_in echoClntAddr; /* Client address */
    unsigned int cliAddrLen; /* Length of incoming message */
    char echoBuffer[ECHOMAX]; /* Buffer for echo string */
    unsigned short echoServPort; /* Server port */
    int recvMsgSize; /* Size of received message */
    int echoStringLen;
    char* serverIP;
    char echoString[ECHOMAX];
    int i = 0,timeSleep;
    char strInt[1];
    echoServPort = 3000; /* First arg:  local port */

    /* Create socket for sending/receiving datagrams */
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        DieWithError("socket() failed");

    /* Construct local address structure */
    memset(&echoServAddr, 0, sizeof (echoServAddr)); /* Zero out structure */
    echoServAddr.sin_family = AF_INET; /* Internet address family */
    echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
    echoServAddr.sin_port = htons(echoServPort); /* Local port */

    /* Bind to the local address */
    if (bind(sock, (struct sockaddr *) &echoServAddr, sizeof (echoServAddr)) < 0)
        DieWithError("bind() failed");
    cliAddrLen = sizeof (echoClntAddr);
    //udp сообщение есть сообщение
    if ((recvMsgSize = recvfrom(sock, echoBuffer, ECHOMAX, 0, (struct sockaddr *) &echoClntAddr, &cliAddrLen)) < 0)
        DieWithError("recvfrom() failed");

    for (;;) /* Run forever */ {

        serverIP = inet_ntoa(echoClntAddr.sin_addr);
        printf("%d Handling client %s\n", i++,  serverIP);

        if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
            DieWithError("socket() failed");

        /* Construct the server address structure */
        memset(&echoServAddr, 0, sizeof (echoServAddr)); /* Zero out structure */
        echoServAddr.sin_family = AF_INET; /* Internet address family */
        echoServAddr.sin_addr.s_addr = inet_addr(serverIP); /* Server IP address */
        echoServAddr.sin_port = htons(6000); /* Server port */

        /* Establish the connection to the echo server */
        if (connect(sock, (struct sockaddr *) &echoServAddr, sizeof (echoServAddr)) < 0)
            DieWithError("connect() failed");
        puts("Connect success");
        strcpy(echoString, "Hello");
        echoStringLen = strlen(echoString); /* Determine input length */

        /* Send the string to the server */
        if (send(sock, echoString, echoStringLen, 0) != echoStringLen)
            DieWithError("send() sent a different number of bytes than expected");
        if ((recvMsgSize = recv(sock, echoBuffer, RCVBUFSIZE, 0)) < 0)
            DieWithError("recv() failed");
        timeSleep = atoi(echoBuffer);
        memmove(strInt, echoBuffer,0);
        
        memmove(echoBuffer, echoBuffer + 1, strlen(echoBuffer));
        printf("%s\n", echoBuffer);
        printf("Sleep client %dsec\n", timeSleep);
        memset(echoBuffer, ' ', 32);
        close(sock);
        sleep(timeSleep);
    }
    /* NOT REACHED */
}

void DieWithError(char *errorMessage) {
    perror(errorMessage);
    exit(1);
}
